import java.io.BufferedReader;
import java.io.EOFException;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class FileReading2 {
	
	public void FileReading2() {
	
	}
	
	public static void main(String args[]) {
	String line = "", fileContent = "";
	String inputin;
	try {
		System.out.println("Masukan nama file...");
		BufferedReader input = new BufferedReader(new InputStreamReader(System.in));
		inputin = input.readLine();
		BufferedReader fileInput = new BufferedReader(new FileReader(new File(inputin)));
		line = fileInput.readLine();
		fileContent = line + "\n";
		while (line != null) {
			line = fileInput.readLine();
			if(line != null)
				fileContent += line + "\n";
		}
		fileInput.close();
	} catch (EOFException eofe) {
		System.out.println("No more lines to read.");
		System.exit(0);
	} catch (IOException ioe) {
		System.out.println("Error in reading file.");
		System.exit(0);
	}
	System.out.println(fileContent);
	}
}