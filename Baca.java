import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.EOFException;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

public class Baca extends JFrame{
	private final JTextField filenames = makePrettyTextField();
	private final JTextArea Data = makePrettyTextArea();
	private final JButton btnCalc = makePrettyButton("Baca File");
	private final Baca self = this;

	public Baca() {
    	super();
    	setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    	setTitle("Baca File");

    	getContentPane().setLayout(new BoxLayout(getContentPane(),
            	BoxLayout.PAGE_AXIS));

    	filenames.setPreferredSize(new Dimension(200,30));
    	filenames.setMaximumSize(filenames.getPreferredSize());
    	getContentPane().setBackground(new Color(232,240,255));
    	getContentPane().add(makePrettyLabel("Masukkan Nama File "));
    	getContentPane().add(Box.createRigidArea(new Dimension(5,5)));
    	getContentPane().add(filenames);
    	getContentPane().add(Box.createRigidArea(new Dimension(5,5)));
    	getContentPane().add(Box.createVerticalGlue());
    	Data.setPreferredSize(new Dimension(200,80));
    	Data.setMaximumSize(filenames.getPreferredSize());
    	getContentPane().add(makePrettyLabel("Isi File"));
    	getContentPane().add(Box.createRigidArea(new Dimension(5,5)));
    	getContentPane().add(Data);
    	getContentPane().add(Box.createRigidArea(new Dimension(5,5)));
    	getContentPane().add(Box.createVerticalGlue());
    	getContentPane().add(btnCalc);
    	getContentPane().add(Box.createRigidArea(new Dimension(5,5)));

    	btnCalc.addActionListener(new ActionListener() {
        	@Override
        	public void actionPerformed(ActionEvent e) {
            	String mass;
            	try {
                	mass = filenames.getText();
               	} catch (Exception ex) {
                	JOptionPane.showMessageDialog(self,
	                	"Please chek your filenames ",
	                	"Input error",
	                	JOptionPane.ERROR_MESSAGE);
                	return;
            	}            	
            	bacafile(mass);
        	}
    	});
    	pack();
    	setVisible(true);
	}

	private JButton makePrettyButton(String title) {
    	JButton button = new JButton(title);
    	button.setFont(new Font(Font.SANS_SERIF, Font.PLAIN, 16));
    	button.setBorder(BorderFactory.createRaisedBevelBorder());
    	button.setBackground(Color.WHITE);
    	button.setForeground(new Color(53,124,255));
    	return button;
	}

	private JTextField makePrettyTextField() {
    	JTextField field = new JTextField();
    	field.setFont(new Font(Font.SANS_SERIF,Font.ITALIC,14));
    	field.setHorizontalAlignment(JTextField.RIGHT);
    	field.setBorder(BorderFactory.createLoweredBevelBorder());
    	return field;
	}
	
	private JTextArea makePrettyTextArea() {
    	JTextArea area = new JTextArea();
    	area.setFont(new Font(Font.SANS_SERIF,Font.ITALIC,14));
    	area.setBorder(BorderFactory.createLoweredBevelBorder());
    	return area;
	}

	private JLabel makePrettyLabel(String title) {
    	JLabel label = new JLabel(title);
    	label.setFont(new Font(Font.SANS_SERIF, Font.BOLD, 14));
    	label.setForeground(new Color(54,124,255));
    	return label;
	}
	
	public void bacafile(String nf) {
		String line ="",fileContent ="";
		
		try {
			BufferedReader fileInput = new BufferedReader(new FileReader(new File(nf)));
			line = fileInput.readLine();
			fileContent = line + "\n";
			while(line != null) {
				line = fileInput.readLine();
				if(line != null) fileContent += line + "\n";
			}
			fileInput.close();
			} catch (EOFException eofe){
				System.out.println("No more lines to read.");
				System.exit(0);
			} catch (IOException ieo) {
				System.out.println("Error reading file.");
				System.exit(0);
			}
		Data.setText(fileContent);
		}			
}