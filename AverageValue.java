import java.awt.*;
import java.awt.event.*;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import javax.swing.*;

public class AverageValue extends JFrame {
	public Button average;
	public JTextField txtValue1,txtValue2,txtValue3,txtValue4,txtValue5;
	public JLabel lbl_Value1,lbl_Value2,lbl_Value3,lbl_Value4, lblHasil,title,lbl_Value5;
	public static double sum;
	private final AverageValue self = this;

	public AverageValue() {
		JFrame frame = new JFrame("Rata-Rata Nilai");
		frame.setSize(300, 400);
		frame.setLayout(null);
		title = new JLabel("Hitung Rata-rata");
		title.setFont(new Font("Arial", Font.BOLD, 18));
		title.setBounds(75, 10, 150, 30);
				
		//Atur posisi lblValue dan txtValue
		lbl_Value5 = new JLabel("Nilai ke 1            : ");
		lbl_Value5.setBounds(25, 50, 100, 30);
		txtValue5 = new JTextField("0");
		txtValue5.setBounds(150, 50, 100, 30);
		txtValue5.setHorizontalAlignment(SwingConstants.CENTER);
		lbl_Value1 = new JLabel("Nilai ke 2            : ");
		lbl_Value1.setBounds(25, 90, 100, 30);
		txtValue1 = new JTextField("0");
		txtValue1.setBounds(150, 90, 100, 30);
		txtValue1.setHorizontalAlignment(SwingConstants.CENTER);
		lbl_Value2 = new JLabel("Nilai ke 3            : ");
		lbl_Value2.setBounds(25, 130, 100, 30);
		txtValue2 = new JTextField("0");
		txtValue2.setBounds(150, 130, 100, 30);
		txtValue2.setHorizontalAlignment(SwingConstants.CENTER);
		lbl_Value3 = new JLabel("Nilai ke 4            : ");
		lbl_Value3.setBounds(25, 170, 100, 30);
		txtValue3 = new JTextField("0");
		txtValue3.setBounds(150, 170, 100, 30);
		txtValue3.setHorizontalAlignment(SwingConstants.CENTER);
		lbl_Value4 = new JLabel("Nilai ke 5            : ");
		lbl_Value4.setBounds(25, 210, 100, 30);
		txtValue4 = new JTextField("0");
		txtValue4.setBounds(150, 210, 100, 30);
		txtValue4.setHorizontalAlignment(SwingConstants.CENTER);
		
		//Atur posisi button
		average = new Button("Average");
		average.setBounds(100, 250, 100, 30);
		
		//Atur posisi hasil
		lblHasil = new JLabel();
		lblHasil.setBounds(100, 300, 100, 30);
		lblHasil.setHorizontalAlignment(SwingConstants.CENTER);
		frame.getContentPane();
		frame.add(title);
		frame.add(lblHasil);
		frame.add(txtValue1);
		frame.add(lbl_Value1);
		frame.add(lbl_Value2);
		frame.add(txtValue2);
		frame.add(lbl_Value3);
		frame.add(txtValue3);
		frame.add(lbl_Value4);
		frame.add(txtValue4);
		frame.add(average);
		frame.add(txtValue5);
		frame.add(lbl_Value5);
		frame.setVisible(true);
	
		//Action Button 
		average.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				sum = 0;
				double v1,v2,v3,v4,v5;
				
				try {
					v1 = Double.parseDouble(txtValue1.getText());
					v2 = Double.parseDouble(txtValue2.getText());
					v3 = Double.parseDouble(txtValue3.getText());
					v4 = Double.parseDouble(txtValue4.getText());
					v5 = Double.parseDouble(txtValue5.getText());
				}catch (NumberFormatException e) {
					JOptionPane.showMessageDialog(self, "Please enter a valid number for value.",
							"Input Error", JOptionPane.ERROR_MESSAGE);
					return;
				}
				double sum = (v1+v2+v3+v4+v5)/5;
				lblHasil.setText(new Double(sum).toString());
			}
		});
	}
	
	public static void main(String[] args) throws IOException {	
		AverageValue rata2 = new AverageValue();
		double rata,totalDev;
		int jumlah;	
		String v1,v2,v3,v4;
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.print("Masukan jumlah data : ");
		jumlah = Integer.parseInt(br.readLine());
		rata = 0;
		String data[] = new String[jumlah];
		for (int j = 0; j < jumlah; j++) {
			System.out.printf("Masukan nilai ke-%d : ",j+1);
			data[j] =  br.readLine();
			rata += (Double.parseDouble(data[j]));
		}
		rata=rata/jumlah;
		System.out.printf("Hasil rata : %.2f%n%n",rata);
		rata2.lblHasil.setText(new Double(rata).toString());
		
		System.out.println("Menghitung deviasi");
		totalDev = 0;
		
		for (int i = 0; i < jumlah; i++) {
			totalDev+=Math.pow(Double.parseDouble(data[i])-rata,2);
		}
		totalDev= Math.sqrt(totalDev/(jumlah-1));
		System.out.printf("Hasil Deviasi Standar = %.2f",totalDev);
	}
}