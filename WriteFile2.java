import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;

public class WriteFile2 {
	
	public WriteFile2() {
		
	}
	
	public static void main(String[] args) throws IOException {
		System.out.println("Masukan Nama File...");
		String filename;
		char temp;
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		filename = br.readLine();
		System.out.println("Masukan teks ke " + filename + "...");
		System.out.println("Ketik ~ untuk berhenti.");
		FileOutputStream fos = null;
		
		try {
			fos = new FileOutputStream(filename);
		} catch (FileNotFoundException ex) {
			System.out.println("File tidak ditemukan.");
		}
		
		try {
			boolean done = false;
			int data;
			do {
				data = br.read();
				if((char) data == '~') {
					done = true;
				} else {
					temp = (char) data; 
					data = (int) Character.toUpperCase(temp);
					fos.write(data);
				}
			} while (!done);
		} catch (IOException ex) {
			System.out.println("Error saat membuka file.");
		}
	}
}