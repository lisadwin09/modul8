import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;

public class Tulis extends JFrame{
	private final JTextField filenames = makePrettyTextField();
	private final JTextArea Data = makePrettyTextArea();
	private final JButton btnCalc = makePrettyButton("Tulis File");
	private final Tulis self = this;

	public Tulis() {
    	super();
    	JOptionPane.showMessageDialog(self, "Enter data to write to file and Type q$ to end.", "Notification", JOptionPane.INFORMATION_MESSAGE);		
    	setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    	setTitle("Tulis File");
    	getContentPane().setLayout(new BoxLayout(getContentPane(), BoxLayout.PAGE_AXIS));
    	filenames.setPreferredSize(new Dimension(200,30));
    	filenames.setMaximumSize(filenames.getPreferredSize());
    	getContentPane().setBackground(new Color(232,240,255));
    	getContentPane().add(makePrettyLabel("Tuliskan Nama File "));
    	getContentPane().add(Box.createRigidArea(new Dimension(5,5)));
    	getContentPane().add(filenames);
    	getContentPane().add(Box.createRigidArea(new Dimension(5,5)));
    	getContentPane().add(Box.createVerticalGlue());
    	getContentPane().add(makePrettyLabel("Masukkan Isi File"));
    	getContentPane().add(Box.createRigidArea(new Dimension(5,5)));
    	getContentPane().add(Data);
    	getContentPane().add(Box.createRigidArea(new Dimension(5,5)));
    	getContentPane().add(Box.createVerticalGlue());
    	getContentPane().add(btnCalc);
    	getContentPane().add(Box.createRigidArea(new Dimension(5,5)));

    	btnCalc.addActionListener(new ActionListener() {
        	@Override
        	public void actionPerformed(ActionEvent e) {
            	String mass;
            	String iisi;
            	
            	try {
                	mass = filenames.getText();
                	iisi = Data.getText();
               	} catch (Exception ex) {
                	JOptionPane.showMessageDialog(self,
                    	"Please chek your information " + ex,
                    	"Input error",
                    	JOptionPane.ERROR_MESSAGE);
                	return;
            	}
            	try {
					tulisfile(mass,iisi);
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					JOptionPane.showMessageDialog(self,
                    	"Gagal " + e1,
                    	"Input error",
                    	JOptionPane.ERROR_MESSAGE);
					return;
				}
        	}
    	});
    	pack();
    	setVisible(true);
	}
	
	private JButton makePrettyButton(String title) {
    	JButton button = new JButton(title);
    	button.setFont(new Font(Font.SANS_SERIF, Font.PLAIN, 16));
    	button.setBorder(BorderFactory.createRaisedBevelBorder());
    	button.setBackground(Color.WHITE);
    	button.setForeground(new Color(53,124,255));
    	return button;
	}

	private JTextField makePrettyTextField() {
    	JTextField field = new JTextField();
    	field.setFont(new Font(Font.SANS_SERIF,Font.ITALIC,14));
    	field.setHorizontalAlignment(JTextField.RIGHT);
    	field.setBorder(BorderFactory.createLoweredBevelBorder());
    	return field;
	}
	
	private JTextArea makePrettyTextArea() {
    	JTextArea area = new JTextArea();
    	area.setFont(new Font(Font.SANS_SERIF,Font.ITALIC,14));
    	area.setBorder(BorderFactory.createLoweredBevelBorder());
    	return area;
	}

	private JLabel makePrettyLabel(String title) {
    	JLabel label = new JLabel(title);
    	label.setFont(new Font(Font.SANS_SERIF, Font.BOLD, 14));
    	label.setForeground(new Color(54,124,255));
    	return label;
	}
	
	public void tulisfile(String nf, String data) throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		nf = br.readLine();	
		FileOutputStream fos = null;
		
		try {
			fos = new FileOutputStream(nf);
		} catch (FileNotFoundException ex) {
			JOptionPane.showMessageDialog(self, "File cannot be opened for writing. " + ex, "Input error", JOptionPane.ERROR_MESSAGE);
			return;
		}
		
		try {
			boolean done = false;
			do {
				int isi = Integer.parseInt(data);
				isi = br.read();
				if ((char)isi == 'q') {
					isi = br.read();
					if ((char)isi == '$') {
						done = true;
					} else {
						fos.write('q');
						fos.write(isi);
					}
				} else {
					fos.write(isi);
				}
			} while (!done);
		} catch (IOException ex) {
			System.out.println("Problem in reading from the file.");
		}
	}			
}