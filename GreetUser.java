import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
public class GreetUser {
//some code
//program menerima sebuah string atau kata
	public static void main(String[] args) throws IOException {
		System.out.println("Hi, what's Your Name ? ");
		String name;
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		name = br.readLine();
		System.out.println("Nice to meet you, " + name + "! :)");
	}
}