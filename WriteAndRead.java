import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import java.awt.event.ActionEvent; 
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.EOFException;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

public class WriteAndRead extends JFrame implements ActionListener {
	private Color color = Color.LIGHT_GRAY;
	private JPanel panel;
	private JPanel panel2;
	private JRadioButton  baca, tulis;
	private JTextField nama,isi;
	private JLabel alamat,data;
	private JButton b,t;
	private String a,d;
	
	public WriteAndRead() {
		panel = new JPanel();
		panel.setBackground ( color );
		getContentPane().setBackground(Color.WHITE);
		baca = new JRadioButton("Baca");
		tulis = new JRadioButton("Tulis");
		panel = new JPanel();
		panel2 = new JPanel();
		panel.add(baca);
		panel.add(tulis);
		getContentPane().add(panel2, BorderLayout.CENTER);
		panel2.setSize(200,200);
		getContentPane().add(panel, BorderLayout.PAGE_END);
	
		// Mengelompokkan radio button
		ButtonGroup group = new ButtonGroup();
		group.add(baca);
		group.add(tulis);
		// Meregistrasi listener
		baca.addActionListener(this);      
		tulis.addActionListener(this);
	}
	
	public void actionPerformed(ActionEvent ae) {
	   if (ae.getSource() == baca) {
		   SwingUtilities.invokeLater(new Runnable() {
	        	@Override
	        	public void run() {
	            	new Baca();
	        	}
	    	});
	   } else if (ae.getSource() == tulis) {
		   SwingUtilities.invokeLater(new Runnable() {
	        	@Override
	        	public void run() {
	            	new Tulis();
	        	}
	    	});
	   }
	}
	
	public static void main(String[] args) {
		WriteAndRead frame = new WriteAndRead();
		frame.setTitle("Baca Dan Tulis File");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setSize(320, 170);
		frame.setVisible(true);   
	}
	
	private JTextField makePrettyTextField() {
    	JTextField field = new JTextField();
    	field.setFont(new Font(Font.SANS_SERIF,Font.ITALIC,14));
    	field.setHorizontalAlignment(JTextField.RIGHT);
    	field.setBorder(BorderFactory.createLoweredBevelBorder());
    	return field;
	}

	private JLabel makePrettyLabel(String title) {
    	JLabel label = new JLabel(title);
    	label.setFont(new Font(Font.SANS_SERIF, Font.BOLD, 14));
    	label.setForeground(new Color(54,124,255));
    	return label;
	}
}