import java.io.FileNotFoundException;
import java.lang.SecurityException;
import java.util.Formatter;
import java.util.FormatterClosedException;
import java.util.NoSuchElementException;
import java.util.Scanner;

public class CreateTextFile {
	private static Formatter output;
	
	public static void main(String[] args) {
		openFile();
		addRecords();
		closeFile();
	}

	public static void openFile() {
		try {
			output = new Formatter("clients.txt");
		} catch (SecurityException se) {
			System.err.println("Write Permission Denied. Terminating..");
			System.exit(1);
		} catch (FileNotFoundException fe) {
			System.err.println("Error opening file. Terminating...");
			System.exit(1);
		}
		
	}
	
	public static void addRecords() {
		Scanner input = new Scanner(System.in);
		System.out.printf("%s%n%s%n?", "Enter Account Number, First Name, Last Name and Balance.", 
				"Enter end-of-file indicator to end input.");
		while(input.hasNext()) {
			try {
				output.format("%d %s %s %.2f%n", input.nextInt(), input.next(), input.next(), input.nextDouble());
			} catch (FormatterClosedException fc) {
				System.err.println("Error writing to file. Terminating...");
				break;
			} catch (NoSuchElementException ne) {
				System.err.println("Invalid input. Please try again");
				input.nextLine();
			}
		}
		
	}

	public static void closeFile() {
		if ( output != null )
			output.close();
		}
}