import java.io.FileNotFoundException;
import java.io.IOException;
import java.lang.SecurityException;
import java.nio.file.Paths;
import java.util.Formatter;
import java.util.FormatterClosedException;
import java.util.NoSuchElementException;
import java.util.Scanner;

public class ReadTextFile {
	
	private static Scanner input;
	
	public static void main(String[] args) throws IOException {
		openFile();
		readRecords();
		closeFile();
	}

	public static void openFile() throws IOException {
		try {
			input = new Scanner(Paths.get("clients.txt"));
		} catch (IOException ie) {
			System.err.println("Error Opening File. Terminating..");
			System.exit(1);
		} 
	}
	
	public static void readRecords() {
		System.out.printf("%-10s%-12s%-12s%10s%n", "Account", "First Name", "Last Name", "Balance");
		try {
			while(input.hasNext()) {
				System.out.printf("%-10s%-12s%-12s%10.2f%n", input.nextInt(), input.next(), input.next(), input.nextDouble());
			}
		} catch (NoSuchElementException ne) {
			System.err.println("File Improperly Formed. Terminating...");
			input.nextLine();
		} catch (IllegalStateException is) {
			System.err.println("Error reading from file. Terminating...");
		}
	}

	public static void closeFile() {
		if (input != null)
		input.close();
	}

}